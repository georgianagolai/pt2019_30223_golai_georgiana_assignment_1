import java.awt.*;
import javax.swing.*;

public class Fereastra {

    public JFrame f;
    public JTextField jtf3, jtf4, jtf5, jtf6,jtf7,jtf9,jtf8,jtf10;
    public JTextField polinom1, polinom2;
    public JButton plus=new JButton("+");
    public JButton minus=new JButton("-");
    public JButton inmultire=new JButton("x");
    public JButton impartire=new JButton("/");
    public JButton derivare=new JButton("'");
    public JButton integrare=new JButton("∫");

    public Fereastra()
    {
        polinom1=new JTextField(17);
        polinom2=new JTextField(17);
        JLabel jl1=new JLabel("Polinom1: ");
        JLabel jl2=new JLabel("Polinom2: ");
        JPanel jp1=new JPanel();
        jp1.add(jl1);
        jp1.add(polinom1); 
        JPanel jp2=new JPanel();
        jp2.add(jl2);
        jp2.add(polinom2);
        
        JPanel jp3=new JPanel();
        jp3.setLayout(new GridLayout(1,6));
        jp3.add(plus);
        jp3.add(minus);
        jp3.add(inmultire);
        jp3.add(impartire);
        jp3.add(derivare);
        jp3.add(integrare);
        
        JLabel jlSuma=new JLabel("Adunare:");
        jtf3=new JTextField();
        jtf3.setPreferredSize(new Dimension(370,30));
        JPanel jp4 = new JPanel();
        jp4.setLayout(new GridLayout(2,1));
        jp4.add(jlSuma);
        jp4.add(jtf3);

        JLabel jlDif=new JLabel("Scadere:");
        jtf4=new JTextField();
        jtf4.setPreferredSize(new Dimension(370,30));
        JPanel jp5 = new JPanel();
        jp5.setLayout(new GridLayout(2,1));
        jp5.add(jlDif);
        jp5.add(jtf4);
     
        JLabel jlInmultire=new JLabel("Inmultire:");
        jtf5=new JTextField();
        jtf5.setPreferredSize(new Dimension(370,30));
        JPanel jp6=new JPanel();
        jp6.setLayout(new GridLayout(2,1));
        jp6.add(jlInmultire);
        jp6.add(jtf5);
      
        JLabel jlImpartire=new JLabel("Impartire:");
        jtf6=new JTextField();
        jtf6.setPreferredSize(new Dimension(370,30));
        JPanel jp7 = new JPanel();
        jp7.setLayout(new GridLayout(2,1));
        jp7.add(jlImpartire);
        jp7.add(jtf6);
        
        JLabel jlDerivare=new JLabel("Derivare:");
        jtf7=new JTextField();
        jtf7.setPreferredSize(new Dimension(370,30));
        jtf8 = new JTextField();
        jtf8.setPreferredSize(new Dimension(370,30));
        JPanel jp8 = new JPanel();
        jp8.setLayout(new GridLayout(3,1));
        jp8.add(jlDerivare);
        jp8.add(jtf7);
        jp8.add(jtf8);
        
        JLabel jlIntegrare=new JLabel("Integrare:");
        jtf9=new JTextField();
        jtf9.setPreferredSize(new Dimension(370,30));
        jtf10 = new JTextField();
        jtf10.setPreferredSize(new Dimension(370,30));
        JPanel jp9 = new JPanel();
        jp9.setLayout(new GridLayout(3,1));
        jp9.add(jlIntegrare);
        jp9.add(jtf9);
        jp9.add(jtf10);
   
        JPanel jp=new JPanel();
        jp.add(jp1);
        jp.add(jp2);
        jp.add(jp3);
        jp.add(jp4);
        jp.add(jp5);
        jp.add(jp6);
        jp.add(jp7);
        jp.add(jp8);
        jp.add(jp9);
        
        plus.addActionListener(new Operatii(this));
        minus.addActionListener(new Operatii(this));
        inmultire.addActionListener(new Operatii(this));
        impartire.addActionListener(new Operatii(this));
        derivare.addActionListener(new Operatii(this));
        integrare.addActionListener(new Operatii(this));
       
        f=new JFrame("Polinoame");
        f.setSize(880,400);
        f.add(jp);
        f.setVisible(true);
       
    }
   
}