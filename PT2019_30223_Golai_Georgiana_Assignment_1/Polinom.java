import java.util.*;

public class Polinom 
{
	ArrayList<Monom> polinom=new ArrayList<Monom>();
	public Polinom() {}
	
	public ArrayList<Polinom> impartire(Polinom p1)
	{
		ArrayList<Polinom>pol=new ArrayList<Polinom>();
		Polinom catul=new Polinom();
		Polinom rest=new Polinom();
		Monom temp=new Monom(0,0,0);
		Polinom p=new Polinom();
		p=this;
		Polinom polin=new Polinom();
		int aux=0;
		int exp=p.getExpMax();
		int exp1=p1.getExpMax();
		if(p.polinom.get(exp).getExp()<p1.polinom.get(exp1).getExp())
		{
			catul.polinom.add(new Monom(0,0,0));
			for(Monom mon:p.polinom)
				rest.polinom.add(mon);
			pol.add(catul);
			pol.add(rest);
			return pol;
		}
		else 
		{
			if(p.polinom.get(exp).getExp()==0)
			{
				if(p1.polinom.get(exp1).getExp()==0)
				{
					temp=p.polinom.get(exp).impartire(p1.polinom.get(exp1));
					catul.polinom.add(temp);
					rest.polinom.add(new Monom(0,0,0));
					catul.addCoefExpEgali();
					rest.addCoefExpEgali();
					pol.add(catul);
					pol.add(rest);
					return pol;
				}
			}
			while(p.polinom.get(exp).getExp()>=p1.polinom.get(exp1).getExp())
			{
				
				polin.polinom.add(p.polinom.get(exp).impartire(p1.polinom.get(exp1)));
				p=p.adunare(p1.monOriMinusPol(polin.polinom.get(aux)));
				exp=p.getExpMax();
				if(p.polinom.size()==0) break;
				aux++;
				if(p.polinom.get(exp).getExp()==0)
				{
					if(p1.polinom.get(exp1).getExp()==0)
					{
						polin.polinom.add(p.polinom.get(exp).impartire(p1.polinom.get(exp1)));
						p=p.adunare(p1.monOriMinusPol(polin.polinom.get(aux)));
						pol.add(polin);
						pol.add(p);
						return pol;
					}
				}
			}
			polin.stergTermCoefZero();
			p.stergTermCoefZero();
			pol.add(polin);
			pol.add(p);
			return pol;
		}
	}
	
	public Polinom inmultire(Polinom p1) 
	{
		Polinom p=new Polinom();
		for(Monom mon:this.polinom)            //sau for(int i=0;i<this.polinom.size();i++) 
		{
			p=p.adunare(p1.inmultirePolCuMon(mon));     //sau p=p.adunare(p1.inmultirePolCuMon(this.polinom.get(i)));	
		}
		p.addCoefExpEgali();
		p.stergTermCoefZero();
		return p;
	}
	
	
	public Polinom monOriMinusPol(Monom m) 
	{
		Polinom p=new Polinom();
		for(Monom mon : this.polinom)  
		{
			Monom m1=mon;
			Monom m2=m1.inmultire(new Monom(0,-1,-1));
			Monom m3=m2.inmultire(m);
			p.polinom.add(m3);	
		}
		p.addCoefExpEgali();
		return p;
	}
	
	public Polinom inmultirePolCuMon(Monom m) 
	{
		Polinom p=new Polinom();
		for(Monom mon:this.polinom) 
		{
			Monom m1=mon;
			Monom m2=m1.inmultire(m);
			p.polinom.add(m2);
			
		}
		p.addCoefExpEgali();
		return p;
	}
	
	public Polinom PolOriMinusUnu(Polinom m) 
	{
		Polinom p=new Polinom();
		for(Monom mon : m.polinom)  
		{
			Monom m1=mon.inmultire(new Monom(0,-1,-1));
			p.polinom.add(m1);
			
		}
		p.addCoefExpEgali();
		return p;
	}
	
	public Polinom adunare(Polinom p1) 
	{
		Polinom p=new Polinom();
		for(int i=0;i<polinom.size();i++) 
		{
			int aux=0;
			Monom m=polinom.get(i);
			int exp=polinom.get(i).getExp();
			for(int j=0;j<p1.polinom.size();j++)
			{
				Monom m1=p1.polinom.get(j);
				int exp1=p1.polinom.get(j).getExp();
				if(exp==exp1)
				{
					p.polinom.add(m.adunare(m1));
					p1.polinom.remove(j);
					aux++;
				}
			}
			if(aux==0)
				p.polinom.add(m);
			
		}
		for(int j=0;j<p1.polinom.size();j++)
		{
			Monom m1=p1.polinom.get(j);
			p.polinom.add(m1);
		}
		p.stergTermCoefZero();
		p.addCoefExpEgali();
		return p;
	}
	
	public Polinom scadere(Polinom p1) 
	{
		p1=PolOriMinusUnu(p1);
		Polinom p=new Polinom();
		for(int i=0;i<polinom.size();i++) 
		{
			int aux=0;
			Monom m=polinom.get(i);
			int exp=polinom.get(i).getExp();
			for(int j=0;j<p1.polinom.size();j++)
			{
				Monom m1=p1.polinom.get(j);
				int exp1=p1.polinom.get(j).getExp();
				if(exp==exp1)
				{
					p.polinom.add(m.adunare(m1));
					p1.polinom.remove(j);
					aux++;
				}
			}
			if(aux==0)
				p.polinom.add(m);
		}
		for(int j=0;j<p1.polinom.size();j++)
		{
			Monom m1=p1.polinom.get(j);
			p.polinom.add(m1);
		}
		p.stergTermCoefZero();
		p.addCoefExpEgali();
		return p;
	}
	
	/*
	 * returneaza indexul exponentului maxim 
	 * */
	
	public int getExpMax()
	{
		int m=-1;
		int exp=-1;
		int i=0;
		for(Monom mon:this.polinom)
		{
			if(exp<mon.getExp())
				{
					exp=mon.getExp();
					m=i;
				}
			i++;
		}
		return m;
	}
	
	public Polinom derivare() 
	{
		Polinom p=new Polinom();
		for(Monom mon:this.polinom)
			p.polinom.add(mon.derivare());
		p.stergTermCoefZero();
		return p;
	}
	
	public Polinom integrare()
	{
		Polinom p=new Polinom();
		for(Monom mon:this.polinom)
			p.polinom.add(mon.integrare());
		p.stergTermCoefZero();
		return p;
	}

	public void addCoefExpEgali()
	{
		for(int i=0;i<polinom.size();i++) 
		{
			int exp=polinom.get(i).getExp();
			for(int j=i+1;j<polinom.size();j++)
			{
				int exp1=polinom.get(j).getExp();
				if(exp==exp1) 
				{
					polinom.get(i).adunareCoef(polinom.get(j).getCoef());
					this.polinom.remove(j);
				}
			}
		}	
	}
	
	public void stergTermCoefZero() 
	{
		for(int i=0;i<polinom.size();i++)
		{
			if(polinom.get(i).coefReal==0) 
				polinom.remove(i);
		}
	}
	
	public String afisare() 
	{
		String s = new String("");
		if(polinom.size()==0)
		{
			s="0.0";
			return s;
		}
		int i=0;
		for(Monom mon:this.polinom)
		{
				if(i==0)
					s=s+mon.getMonom();
				else
					if(mon.coefReal < 0) 
						s=s+mon.getMonom();
					else
						s=s+"+"+mon.getMonom();	
				i++;
		}
		return s;
	}
}