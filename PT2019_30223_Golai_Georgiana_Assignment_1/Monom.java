import java.util.regex.*;

public class Monom
{
	public String monom;
	public int exponent;
	public int coef;
	public float coefReal;

	public Monom(int exponent,int coef,float coefReal)
	{
		this.coef=coef;
		this.exponent=exponent;
		this.coefReal=coefReal;
	}
	
	public Monom(String monom) 
	{
		if(verificareMonom(monom)!=-1)  
			this.monom=monom;
		coefReal=(float)coef;
	}
	
	public int getExp()
	{
		return this.exponent;
	}
	
	public int getCoef()
	{
		return this.coef;
	}

	public String getMonom() 
	{
		if(exponent!=0)
			return coefReal+"x^"+exponent;
		return Float.toString(coefReal);
	}
	
	public Monom adunare(Monom mon1)
	{
			float coefReal=this.coefReal+mon1.coefReal;
			int coef=(int)coefReal;
			int exp=this.getExp();
			return new Monom(exp,coef,coefReal);
	}
	
	public Monom inmultire(Monom mon1)
	{
		float coefReal=this.coefReal*mon1.coefReal;
		int coef=(int)coefReal;
		int exp=this.getExp()+mon1.getExp();
		return new Monom(exp,coef,coefReal);
	}
	
	public Monom impartire(Monom divizor)
	{
		float coefReal=this.coefReal/divizor.coefReal;
		int coef=(int)coefReal;
		int exp=this.getExp()-divizor.getExp();
		return new Monom(exp,coef,coefReal);
	}
	
	public Monom derivare()
	{
		if (exponent != 0)
		{
			coefReal=coefReal*exponent;
			coef=(int)coefReal;
			exponent=exponent-1;
			return new Monom(exponent,coef,coefReal);
		}
		else 
		{
			coefReal=0;
			coef=0;
			exponent=0;
			return new Monom(exponent,coef,coefReal);
		}
	}
	
	public Monom integrare()
	{
		exponent=exponent+1;
		coefReal=coefReal / exponent;
		coef=(int)coefReal;
		return new Monom(exponent,coef,coefReal);	
	}
	
	public void adunareCoef(int x)
	{
		this.coef=this.coef+x;
		this.coefReal=(float)coef;
	}
	
	public int verificareMonom(String monom)
	{
		String[] str;
		if(!monom.matches("^[a-zA-Z0-9\\^\\-]*")) 
		{
			System.out.println("date gresite");
			return -1;
		}
		Pattern pattern=Pattern.compile("\\^");
		Matcher matcher=pattern.matcher(monom);
		String s=new String();
		while(matcher.find())
		{
			s=matcher.group();
		}
		if(s.length()==0)
		{
			str=monom.split("[a-zA-Z]");
			if(str.length==0)
			{
				coef=1;
				exponent=1;
			} else
			{
				if(str[0].equals("-"))
				{
					coef=-1;
				}
				else
				    coef=Integer.parseInt(str[0]);
				if(str[0]!=monom)
					exponent=1;
				else
					exponent=0;
			}
		} 
		else
		{
			str=monom.split("\\^");
				String st=new String();
				for(int i=0;i<str[0].length();i++){
			        char c=str[0].charAt(i);
			        if(c=='-') 
			        	st=st+c;
			        if(c=='0'||c=='1'||c=='2'||c=='3'||c=='4'||c=='5'||c=='6'||c=='7'||c=='8'||c=='9')
			        	st=st+c;
			    }
				if(st.length()!=0)
					if(st.equals("-"))
						coef=-1;
					else
						coef=Integer.parseInt(st);
				else
					coef=1;
				exponent=Integer.parseInt(str[1]);
		} 	 
		return 0;
	}
}