import java.awt.event.*;
import java.util.*;

public class Operatii implements ActionListener
{
	Fereastra f;
	public Operatii (Fereastra f)
	{
		this.f=f;
	}
	
	public Polinom creare(String s)
    {
		Polinom p=new Polinom();
    	String monom[];
    	String s1,s2;
    	if(s.contains("-"))
    		s1=s.replace("-", "+-");
    	else
    		s1=s;
    	if(s1.charAt(0)=='+')
    		s2=s1.substring(1);
    	else
    		s2=s1;
       	monom=s2.split("\\+");
    	for(int i=0;i<monom.length;i++)
    	{
    		p.polinom.add(new Monom(monom[i]));
   		}
   		p.addCoefExpEgali();
       	return p;
    }
	
    public void actionPerformed(ActionEvent ev)
    {
    	Object sursa=ev.getSource(); 
		if(sursa==f.plus)
		{
			String s1=f.polinom1.getText();
            String s2=f.polinom2.getText();
            Polinom p1=creare(s1);
        	Polinom p2=creare(s2);
        	Polinom p=p1.adunare(p2);
        	f.jtf3.setText(p.afisare());
		}
         else if(sursa==f.minus) 
         {
            String s1=f.polinom1.getText();
            String s2=f.polinom2.getText();
            Polinom p1=creare(s1);
        	Polinom p2=creare(s2);
        	Polinom p=p1.scadere(p2);
        	f.jtf4.setText(p.afisare());
         } 
         else if(sursa==f.inmultire)
         {
            String s1=f.polinom1.getText();
            String s2=f.polinom2.getText();
            Polinom p1=creare(s1);
        	Polinom p2=creare(s2);
        	Polinom p=p1.inmultire(p2);
        	f.jtf5.setText(p.afisare());
         } 
         else if(sursa==f.impartire)
         {
        	String s1=f.polinom1.getText();
            String s2=f.polinom2.getText();
            ArrayList<Polinom> p=new ArrayList<Polinom>();
        	Polinom p1=creare(s1);
        	Polinom p2=creare(s2);
        	p=p1.impartire(p2);
            f.jtf6.setText(" cat: "+p.get(0).afisare() + "       rest: " + p.get(1).afisare());
         }
        else if(sursa==f.derivare)
        {
            String s1=f.polinom1.getText();
            String s2=f.polinom2.getText();
            Polinom p1=creare(s1);
    		Polinom p=p1.derivare();
    		Polinom p2=creare(s2);
    		Polinom p3=p2.derivare();
        	f.jtf7.setText(p.afisare());
        	f.jtf8.setText(p3.afisare());
        } 
        else if(sursa==f.integrare)
        {
            String s1=f.polinom1.getText();
            String s2=f.polinom2.getText();
            Polinom p1=creare(s1);
    		Polinom p=p1.integrare();
    		Polinom p2=creare(s2);
    		Polinom p3=p2.integrare();
        	f.jtf9.setText(p.afisare());
        	f.jtf10.setText(p3.afisare());
        } 
    }	    
}