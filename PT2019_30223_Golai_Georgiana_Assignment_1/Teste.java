import org.junit.Test;
import static org.junit.Assert.*;
public class Teste {

	Polinom p=new Polinom();
	Polinom p1=new Polinom();
	@Test
	public void testAdunare()
	{
		String s="2.0x^1";
		p.polinom.add(new Monom(1,1,1));
		p1.polinom.add(new Monom(1,1,1));
		assertEquals(p.adunare(p1).afisare(),s); 
		System.out.println("Adunarea s-a efectuat cu succes");	
	}
	
	@Test
	public void testScadere()
	{
		String s="0.0";
		p.polinom.add(new Monom(1,1,1));
		p1.polinom.add(new Monom(1,1,1));
		assertEquals(p.scadere(p1).afisare(),s); 
		System.out.println("Scaderea s-a efectuat cu succes");
	}
	
	@Test
	public void testInmultire()
	{
		String s="1.0x^2";
		p.polinom.add(new Monom(1,1,1));
		p1.polinom.add(new Monom(1,1,1));
		assertEquals(p.inmultire(p1).afisare(),s);
		System.out.println("Inmultirea s-a efectuat cu succes");
	}
	
	@Test
	public void testImpartire()
	{
		String s="1.0";
		String s1="0.0";
		p.polinom.add(new Monom(1,1,1));
		p1.polinom.add(new Monom(1,1,1));
		assertEquals(p.impartire(p1).get(0).afisare(),s);
		assertEquals(p.impartire(p1).get(1).afisare(),s1);
		System.out.println("Impartirea s-a efectuat cu succes");
	}
	
	@Test
	public void testDerivare()
	{
		String s="1.0";
		p.polinom.add(new Monom(1,1,1));
		assertEquals(p.derivare().afisare(),s); 
		System.out.println("Deivarea s-a efectuat cu succes");
	}
	
	@Test
	public void testIntegrare()
	{
		String s="0.5x^2";
		p.polinom.add(new Monom(1,1,1));
		assertEquals(p.integrare().afisare(),s); 
		System.out.println("Integrarea s-a efectuat cu succes");
	}

}